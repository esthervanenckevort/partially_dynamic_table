//
//  ViewController.swift
//  PartiallyDynamicTable
//
//  Created by David van Enckevort on 05/02/2019.
//  Copyright © 2019 All Things Digital. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    let cellId = String(describing: DynamicTableViewCell.self)

    let images: [(image: UIImage, description: String)] = [
        (image: UIImage(named: "cat1")!, description: "Cute cat image from Pixabay"),
        (image: UIImage(named: "cat2")!, description: "Another cute cat image from Pixabay")
    ]

    override func viewDidLoad() {
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableView.dataSource = self
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return images.count
        } else {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DynamicTableViewCell.self), for: indexPath) as! DynamicTableViewCell
            cell.model = images[indexPath.row]
            return cell
        } else {
            return super.tableView(tableView, cellForRowAt: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 {
            return 116
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }

    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        if indexPath.section == 2 {
            return 0
        } else {
            return super.tableView(tableView, indentationLevelForRowAt: indexPath)
        }
    }
}


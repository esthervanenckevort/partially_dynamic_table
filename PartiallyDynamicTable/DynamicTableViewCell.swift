//
//  DynamicTableViewCell.swift
//  PartiallyDynamicTable
//
//  Created by David van Enckevort on 05/02/2019.
//  Copyright © 2019 All Things Digital. All rights reserved.
//

import UIKit

class DynamicTableViewCell: UITableViewCell {

    @IBOutlet private var photo: UIImageView!
    @IBOutlet private var photoDescription: UITextView!

    var model: (image: UIImage, description: String)? {
        didSet {
            photo?.image = model?.image
            photoDescription?.text = model?.description
        }
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        photo?.image = model?.image
        photoDescription?.text = model?.description
    }
}
